# rust + warp Example

This is a very simple example which demonstrates how we can build some
RESTful app in rust with the warp library.

## Prerequisites

- git
- docker
- docker-compose

## Quick Start

First, use git clone to download this repository to your local machine.

Then simply change to the top level directory after checkout and issue the
following command:

```
$ docker-compose up -d
```

Finally, open your web browser and point to the following links:

- [logo](http://[::]:8080/)
- [hello](http://[::]:8080/hello/Joe)
- [hina](http://[::]:8080/hina/test_me)

