extern crate hinafont;

use warp::Filter;

#[tokio::main]
async fn main()
{
	let logo = warp::get()
		.and(warp::path::end())
		.map(|| hinafont::conv("rust + warp"));

	let hello = warp::path!("hello" / String)
		.map(|name| format!("Hello, {}!", name));

	let hina = warp::path!("hina" / String)
		.map(|s| hinafont::conv(&format!("{}", s)));

	warp::serve(logo.or(hello).or(hina))
		.run(([0, 0, 0, 0], 80))
		.await;
}
